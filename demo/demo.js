import '@bbva-web-components/bbva-foundations-theme/bbva-foundations-theme.js';
import '@cells-components/coronita-icons/coronita-icons.js';
import './css/demo-styles.js';
import '../cells-ui-login-vcard.js';

// Include below here your components only for demo
// import 'other-component.js'