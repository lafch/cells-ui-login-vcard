import { LitElement, html, css } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import styles from './cells-ui-login-vcard-styles.js';
import '@vcard-components/cells-util-behavior-vcard';
import '@bbva-web-components/bbva-form-field';
import '@bbva-web-components/bbva-button-default';
import '@bbva-web-components/bbva-form-password';
import '@vcard-components/cells-ui-message-alert-vcard';

const cellsUtilBehaviorVcard = CellsBehaviors.cellsUtilBehaviorVcard;
/**
This component ...

Example:

```html
<cells-ui-login-vcard></cells-ui-login-vcard>
```

##styling-doc

* @customElement
* @polymer
* @LitElement
* @demo demo/index.html
*/
export class CellsUiLoginVcard extends cellsUtilBehaviorVcard(LitElement) {
  static get is() {
    return 'cells-ui-login-vcard';
  }

  // Declare properties
  static get properties() {
    return {
      user: { 
        type: String
      },
      password: { 
        type: String 
      },
      loading: {
        type: Boolean
      }

    };
  }

  // Initialize properties
  constructor() {
    super();
    console.log(this.ctts);
    this.user="";
    this.password="";
    this.shadowRoot.addEventListener('keypress', (e)=> {
      let keycode = (e.keyCode ? e.keyCode : e.which);
      if (keycode === 13) {
        this.login();
        e.preventDefault();
        return false;
    }
    });
  }

  updateValue(evento){
    if(this.getInputValue('txtUsuario') && this.getInputValue('txtPassword') ) {
      this.getById('btnAcceso').disabled = false;
    } else {
      this.getById('btnAcceso').disabled = true;
    }
  }

  login(evento){
    if (this.getById('btnAcceso').disabled) {
      return;
    }
    let path = this.services.endPoints.login;
    let settingsDm = {
      method: 'POST',
      requiresTokenConfig: true,
      path: path,
      body: this.bodyLogin(),
      onSuccess: this.loginResponse.bind(this),
      onError:(error)=> { 
                          this.showAlert(
                                          { 
                                            delay:5000,
                                            message: error.responseError && error.responseError.message ? error.responseError.message : 'Error al obtener las credeciales de acceso.' 
                                          }
                                        ); }
    };
    this.dispatch(this.events.loginAutenticationRequest, settingsDm);
  }

  showAlert(alert) {
    let alertMessage = this.shadowRoot.querySelector('cells-ui-message-alert-vcard');
    alertMessage.add(alert);
  }

  bodyLogin(){
    let login = new Object();
    login.username = this.getInputValue('txtUsuario');
    login.password = this.getInputValue('txtPassword');
    return login;
  }

  loginResponse(evento){
    let detail = evento.detail;
    this.dispatch(this.events.loginSuccess, detail);
  }

  reset() {
    this.getById('txtUsuario').value = '';
    this.getById('txtPassword').value = '';
  }

  static get shadyStyles() {
    return `
      ${styles.cssText}
      ${getComponentSharedStyles('ui-login-vcard-shared-styles').cssText}
    `;
  }

  // Define a template
  render() {
    return html`
      <style>
      ${this.constructor.shadyStyles}
      </style>
      <slot></slot>
      
      <div class = "body-login" style = "background-image: url(${this.ctts.loginBackgound ? this.ctts.loginBackgound : ''})" >
        <div class="container" style = "position:relative">
          <div class='d-flex justify-content-center h-100'>
            
          <div class='card'>
                <div class='card-header'>
                  <div class='logo'> <img src='${this.ctts.logoBlancoBbva ? this.ctts.logoBlancoBbva : ''}' width='100' height='50'></img> </div>
                  <h3>¡Bienvenido!</h3>
                </div>
                <div class='card-body'>
                
                  <div class='input-group'>
                    <bbva-form-field     id='txtUsuario'  label='Usuario'  @input = "${this.updateValue}" ></bbva-form-field>
                  </div>
                  <div class='input-group'>
                    <bbva-form-password  id='txtPassword' label='Clave de Acceso'  @input="${this.updateValue}" ></bbva-form-password>
                  </div>
                  <div class='input-group text-center' >
                    <bbva-button-default id='btnAcceso'  @click='${this.login}' text='Acceso' disabled></bbva-button-default>
                  </div>

                  <div ?hidden = "${!this.loading}" class="progress-line"></div>

                </div>
            </div>
          </div>
          <cells-ui-message-alert-vcard position="top" ></cells-ui-message-alert-vcard>

        </div>
      </div>
    `;
  }
}

// Register the element with the browser
customElements.define(CellsUiLoginVcard.is, CellsUiLoginVcard);
